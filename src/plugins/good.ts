import { createEventLogStream } from '@dk/module-logger';
const good = require('@hapi/good'); // this module have no type declaration yet

import logger from '../logger';

const goodOptions = {
  ops: false,
  reporters: {
    loggerReporter: [createEventLogStream(logger)]
  }
};

export default {
  plugin: good,
  options: goodOptions
};
