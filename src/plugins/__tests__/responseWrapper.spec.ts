import hapi from '@hapi/hapi';
import Joi from '@hapi/joi';

import responseWrapper from '../responseWrapper';
import { AppError } from '../../errors/AppError';
import { ERROR_CODE, ErrorList } from '../../common/errors';
import { Http } from '@dk/module-common/';

describe('Plugin - responseWrapper', () => {
  const testServer = new hapi.Server();
  const testHandler = jest.fn();
  testServer.route([
    {
      method: Http.Method.POST,
      path: '/test',
      options: {
        handler: testHandler,
        validate: {
          payload: Joi.object({
            name: Joi.string()
          })
        }
      }
    },
    {
      method: Http.Method.GET,
      path: '/documentation',
      options: {
        handler: () => 'documentation'
      }
    }
  ]);

  testServer.register(responseWrapper);

  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('handleHapiResponse', () => {
    it('should wrap success response in data object', async () => {
      testHandler.mockResolvedValueOnce('test result');

      const response: hapi.ServerInjectResponse = await testServer.inject({
        method: Http.Method.POST,
        url: '/test',
        payload: {
          name: 'test'
        }
      });

      expect(response.result).toEqual({
        data: 'test result'
      });
    });

    it('should wrap AppError response in error object', async () => {
      testHandler.mockRejectedValueOnce(
        new AppError(ERROR_CODE.USER_NAME_EXISTED)
      );

      const response: hapi.ServerInjectResponse = await testServer.inject({
        method: Http.Method.POST,
        url: '/test',
        payload: {
          name: 'test'
        }
      });

      expect(response.result).toEqual({
        error: {
          code: ERROR_CODE.USER_NAME_EXISTED,
          message: ErrorList[ERROR_CODE.USER_NAME_EXISTED].message
        }
      });
      expect(response.statusCode).toEqual(
        ErrorList[ERROR_CODE.USER_NAME_EXISTED].statusCode
      );
    });

    it('should wrap 500 error response in error object', async () => {
      testHandler.mockRejectedValueOnce('unexpected error');

      const response: hapi.ServerInjectResponse = await testServer.inject({
        method: Http.Method.POST,
        url: '/test',
        payload: {
          name: 'test'
        }
      });

      expect(response.result).toEqual({
        error: {
          code: 'Internal Server Error',
          message: 'An internal server error occurred'
        }
      });
      expect(response.statusCode).toEqual(500);
    });

    it('should throw normal error on not server error', async () => {
      const response: hapi.ServerInjectResponse = await testServer.inject({
        method: Http.Method.POST,
        url: '/test',
        payload: {
          unwanted: 'error'
        }
      });

      expect(response.result).toEqual({
        error: {
          code: 'Bad Request',
          message: 'Invalid request payload input'
        }
      });
      expect(response.statusCode).toEqual(400);
    });

    it('should ignore document path', async () => {
      const response: hapi.ServerInjectResponse = await testServer.inject({
        method: Http.Method.GET,
        url: '/documentation'
      });

      expect(response.result).not.toHaveProperty('data');
      expect(response.result).not.toHaveProperty('error');
    });
  });
});
