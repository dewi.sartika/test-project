/* istanbul ignore file */ // TODO this will be removed. Jest got a bug w.r.t coverage on some scearios like object destructuring
import { createConnection } from 'typeorm';
import * as entities from '../entities';
import logger from '../logger';
import { config } from '../config';
import { ConfigParameter } from './constant';

const init = async () => {
  const db = config.get(ConfigParameter.db);
  const { host, port, username, database, type, synchronize } = db;
  logger.info('Connect SQL server');
  return await createConnection({
    host,
    port,
    type,
    database,
    username,
    password: process.env.SQL_PASSWORD,
    synchronize,
    entities: Object.values(entities)
  });
};

const SqlDb = {
  init
};

export default SqlDb;
